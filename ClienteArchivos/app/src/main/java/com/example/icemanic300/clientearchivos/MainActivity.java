package com.example.icemanic300.clientearchivos;

import android.app.Dialog;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    private final int PICKER = 1;
    Button btnAbrir, btnManipular, btnCrearArchivos, btnConectar;
    String rutaArchivo;
    File original;
    TextView txtOriginal, txtResultado;

    //Para conectarse con el servidor
    Socket clientePeticion = null;
    PrintWriter alServidor;
    DataInputStream delServidor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAbrir = findViewById(R.id.btnAbrir);
        btnManipular = findViewById(R.id.btnManipular);
        btnConectar = findViewById(R.id.btnConectar);
        btnCrearArchivos = findViewById(R.id.btnCrearArchivos);
        btnManipular.setEnabled(false);
        btnAbrir.setEnabled(false);
        txtOriginal = findViewById(R.id.txtOriginal);
        txtResultado = findViewById(R.id.txtResultado);

        btnAbrir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbrirArchivo();
            }
        });

        btnCrearArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                desplegarCrearArchivos(null);
            }
        });

        btnManipular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogoOpciones = new Dialog(MainActivity.this);
                dialogoOpciones.setTitle("¿Qué desea hacer?");
                dialogoOpciones.setContentView(R.layout.menu_opciones);

                RadioButton rbtnInvertir = dialogoOpciones.findViewById(R.id.rbtnInvertir);
                rbtnInvertir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogoOpciones.dismiss();
                        Toast.makeText(getApplicationContext(),
                                "Selecciono Invertir", Toast.LENGTH_SHORT).show();
                    }
                });

                RadioButton rbtnInvertirLineas = dialogoOpciones.findViewById(R.id.rbtnInvertirLineas);
                rbtnInvertirLineas.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogoOpciones.dismiss();
                        Toast.makeText(getApplicationContext(),
                                "Selecciono Invertir Lineas", Toast.LENGTH_SHORT).show();
                    }
                });

                RadioButton rbtnAmbos = dialogoOpciones.findViewById(R.id.rbtnAmbos);
                rbtnAmbos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogoOpciones.dismiss();
                        Toast.makeText(getApplicationContext(),
                                "Selecciono Invertir Todo", Toast.LENGTH_SHORT).show();
                    }
                });

                dialogoOpciones.show();
            }
        });

        btnConectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogoDatosServidor = new Dialog(MainActivity.this);
                dialogoDatosServidor.setTitle("Parámetros de Conexión");
                dialogoDatosServidor.setContentView(R.layout.datos_servidor);

                Button btnAceptar = dialogoDatosServidor.findViewById(R.id.btnAceptar);
                final TextView txtIP = dialogoDatosServidor.findViewById(R.id.txtIp);
                final TextView txtPuerto = dialogoDatosServidor.findViewById(R.id.txtPuerto);

                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (txtIP.getText().length() > 0){
                            if (txtPuerto.getText().length() > 0){
                                String direccionIP = txtIP.getText().toString();
                                int puertoServidor = Integer.parseInt(txtPuerto.getText().toString());
                                try {
                                    clientePeticion = new Socket(direccionIP, puertoServidor);
                                    alServidor = new PrintWriter(clientePeticion.getOutputStream(),true);
                                    delServidor = new DataInputStream(clientePeticion.getInputStream());
                                    Toast.makeText(MainActivity.this, "Conexion Establecida",Toast.LENGTH_LONG).show();
                                    btnManipular.setEnabled(true);
                                    btnAbrir.setEnabled(true);
                                }catch (Exception ex){
                                    Toast.makeText(MainActivity.this, "Error al Conectar",Toast.LENGTH_LONG).show();
                                }

                                dialogoDatosServidor.dismiss();
                            }else{
                                Toast.makeText(MainActivity.this, "Ingrese el Puerto", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(MainActivity.this, "Ingrese IP", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                dialogoDatosServidor.show();
            }
        });
    }

    @Override
    protected void OnDestroy(){

    }

    private void AbrirArchivo() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("txt/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Seleccionar Archivo"), PICKER);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Instale Manejador de Archivos", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        BufferedReader lector = null;
        try {
            switch (requestCode) {
                case PICKER:
                    if (resultCode == RESULT_OK) {
                        rutaArchivo = data.getData().getPath();
                        original = Environment.getExternalStorageDirectory();
                        lector = new BufferedReader(new FileReader(rutaArchivo));
                        String cadenaAuxiliar;
                        cadenaAuxiliar = lector.readLine();
                        while (cadenaAuxiliar != null) {
                            txtOriginal.append(cadenaAuxiliar + "\n");
                            cadenaAuxiliar = lector.readLine();
                        }
                        lector.close();
                        btnManipular.setEnabled(true);
                    }
                    if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(this, "Por favor, seleccione un archivo", Toast.LENGTH_LONG).show();
                    }
                    break;
            }

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void desplegarCrearArchivos(View vista){
        Intent cambiar = new Intent(this, CrearArchivos.class);
        startActivity(cambiar);
    }

}
